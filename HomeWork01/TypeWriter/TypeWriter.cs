﻿using System;
using TypeWriter.Core;

namespace TypeWriter
{
    public static class TypeWriter
    {
        private readonly static string _message = "Съешь ещё этих мягких французских булок";
        private readonly static string _yes = "Да";

        public static void Execute()
        {
            TyperWriterHandler typerWriterHandler = new TyperWriterHandler();
            typerWriterHandler.CheckTypingSpeed(_message);
            
            string reRunQuestionMessage = "Попробовать еще раз?" 
                             + Environment.NewLine 
                             + "Для продолжения введите \"Да\", для выхода любую клавишу";
            Console.WriteLine(reRunQuestionMessage);
            
            string response = Console.ReadLine();
            if (response == _yes)
            {
                Execute();
            }
        }
    }
}