﻿using System;

namespace TypeWriter.Core
{
    public class TyperWriterHandler
    {
        public void CheckTypingSpeed(string message)
        {
            Console.WriteLine($"Напечатайте следующий текст: " 
                              + Environment.NewLine 
                              + $"\"{message}\"");

            DateTime startTime = DateTime.Now;

            Console.ReadLine();

            TimeSpan resultTime = DateTime.Now - startTime;
            string resultStr = String.Format("{0:00}:{1:00}:{2:00}"
                                            , resultTime.Hours, resultTime.Minutes, resultTime.Seconds);
            
            Console.WriteLine($"{resultStr}");
        }
    }
}